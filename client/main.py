import time
import requests
import json
import random


def requete_serveur(liste_artistes):
    # choisi dans une liste de dictionnaires ayant pour clés artiste et note
    # un artiste au hasard ayant une note supérieure à 15
    # en interrogeant le serveur, renvoie un titre aléatoire
    # un dictionnaire ayant pour clés "artist", "title",
    # "suggested_youtube_url","lyrics":
    url = "http://127.0.0.1:8000/random/"
    indice_artiste_choisi = random.randrange(len(liste_artistes))
    while liste_artistes[indice_artiste_choisi]["note"] < 15:
        indice_artiste_choisi = random.randrange(len(liste_artistes))
    url_complete = url + liste_artistes[indice_artiste_choisi]["artiste"]
    code = 0
    compteur = 0
    while code != 200:
        r = requests.get(url_complete)
        reponse = r.json()
        code = r.status_code
        if code != 200:
            time.sleep(1)
            print("Erreur serveur sur requête " + url +
                  ". Nouvel essai dans 1 seconde")
        compteur += 1
        if compteur > 10:
            reponse = "erreur"
    return reponse


with open("rudy.json", "r") as fichier:
    rudy = json.load(fichier)
reponses = []
for i in range(1, 20):
    reponses.append(requete_serveur(rudy))
print(reponses)
