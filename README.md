# examenConceptionLogicielle

Examen de Conception Logicielle ENSAI 2022


## Quickstart

### installation

```shell
git clone https://gitlab.com/noeldebarle/examenconceptionlogicielle.git
cd examenConceptionLogicielle
pip install -r requirements.txt
```

### utilisation

Le client ne fonctionne que si le serveur est déjà lancé.

Lancement du serveur (de la racine du projet)
``` shell
cd serveur
uvicorn main:app
```

Lancement du client (de la racine du projet)
``` shell
cd client
uvicorn main:app
```

## Schéma d'architecture
```mermaid
graph TD
    A[Client Python] -->|http| B[Serveur Python]
    B --> |json| A
    B --> |http| C[AudioDB]
    C --> |json| B
    B --> |http| D[Lyrics OVH]
    D --> |json| B

```
