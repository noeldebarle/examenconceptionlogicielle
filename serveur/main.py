from fastapi import FastAPI, Depends
from fastapi_health import health
import requests
import random
import time


def requete(url):
    # Fonction permettant de renouveler les requêtes
    # en cas de non réponse de l'API
    code = 0
    compteur = 0
    while code != 200:
        r = requests.get(url)
        code = r.status_code
        if code != 200:
            time.sleep(1)
            print("Erreur serveur sur requête " + url +
                  ". Nouvel essai dans 1 seconde")
        compteur += 1
        if compteur > 10:
            break
    return r


def check_API():
    r = requests.get(
        'https://www.theaudiodb.com/api/v1/json/2/search.php?s=rick%20astley')
    return (r.status_code == 200)


def is_API_online(session: bool = Depends(check_API)):
    return session


app = FastAPI()
app.add_api_route("/", health([is_API_online]))


@app.get("/random/{artist_name}")
def read_item(artist_name: str):
    reponse = {}
    # Récupération de l'id de l'artiste
    url = "https://www.theaudiodb.com/api/v1/json/2/search.php?s="
    url = url + artist_name
    r = requete(url)
    try:
        id_artiste = r.json()["artists"][0]["idArtist"]
    except Exception:
        id_artiste = ""
    reponse["artist"] = r.json()["artists"][0]["strArtist"]
    # récupération de l'ensemble des albums
    url = "https://theaudiodb.com/api/v1/json/2/album.php?i="
    url = url + id_artiste
    r = requete(url)
    # choix d'un album
    try:
        nombre_albums = len(r.json()["album"])
    except Exception:
        nombre_albums = 0
    rang_album_choisi = random.randrange(nombre_albums)
    try:
        id_album = r.json()["album"][rang_album_choisi]["idAlbum"]
    except Exception:
        id_album = ""
    # récupération de l'ensemble des titres de l'album
    url = "https://theaudiodb.com/api/v1/json/2/track.php?m="
    url = url + id_album
    r = requete(url)
    # choix d'un titre dans l'album
    try:
        nombre_titres = len(r.json()["track"])
    except Exception:
        nombre_titres = 0
    rang = random.randrange(nombre_titres)
    try:
        reponse["title"] = r.json()["track"][rang]["strTrack"]
    except Exception:
        reponse["title"] = ""
    try:
        reponse["suggested_youtube_url"] = r.json()["track"][rang]["strMusicVid"]
    except Exception:
        reponse["suggested_youtube_url"] = ""
    # récupération des paroles
    url = "https://api.lyrics.ovh/v1/"
    url = url + reponse["artist"] + "/" + reponse["title"]
    r = requete(url)
    try:
        reponse["lyrics"] = r.json()["lyrics"]
    except Exception:
        reponse["lyrics"] = ""
    print("Envoi de " + reponse["title"] + " de " + reponse["artist"])
    return reponse
